// Домашня робота:
// 1 Створення класу та об'єкта:
// Створіть клас Person з властивостями name та age.
// Створіть об'єкт з цього класу з ім'ям "John" та віком 30.

// class Person{
//     constructor(name, surnames, age, profession){
//         this._name = name;
//         this._surnames = surnames;
//         this._age = age;
//         this._profession = profession;
//     }
// }
// let info = new Person('Yevhen', 'Tuma', 29, 'Front-End-Developer');
// console.log(info);




// 2 Методи класу:
// Розширте клас Person, додайте метод sayHello, який виводить повідомлення з привітанням та ім'ям особи в console.log

// class Person{
//     constructor(name, surnames, age, profession){
//         this._name = name;
//         this._surnames = surnames;
//         this._age = age;
//         this._profession = profession;
//     }
//     seyHello(){
//         console.log(`Hello User ${this._name}`);
//     }
// }
// let info = new Person('Yevhen', 'Tuma', 29, 'Front-End-Developer');
// console.log(info);
// info.seyHello();





// 3 Наслідування:
// Створіть клас Student, який наслідує від класу Person.
// Додайте властивість studentId до класу Student.
// Додайте метод study, який виводить повідомлення про студента

// class Person{
//         constructor(name, surnames, age, profession){
//             this._name = name;
//             this._surnames = surnames;
//             this._age = age;
//             this._profession = profession;
//         }
//     }
//     let info = new Person('Yevhen', 'Tuma', 29, 'Front-End-Developer');
//     console.log(info);

// class Student extends Person{
//     constructor(name, surnames, age, profession, studentId){
//         super(name, surnames, age, profession);
//         this._studentId = studentId;
//     }
//     study(){
//         console.log(`Name ${this._name}, Surnames ${this._surnames}, Age ${this._age}, Profession ${this._profession}, StudentId ${this._studentId}`)
//     }
// }    
// let id = new Student('Yevhen', 'Tuma', 29, 'Front-End-Developer', 16655);
// console.log(id);
// id.study();






// 4 Задачі для практики forEach, reverse  Виведіть значення з масиву в зворотньому порядку
// const numbers = [1, 2, 3, 4, 5];

// Тут трохи не зрозумів куди додоти reverse.

// const numbers = [1, 2, 3, 4, 5];
// numbers.reverse();
// numbers.forEach((number) => {
//     console.log({number});
    
// });





// 5  Задачі для практики map та Math.round
//  Маєте масив чисел з плаваючою комою. Використовуйте map разом з Math.round, щоб створити новий масив, в якому кожне число буде округлене до найближчого цілого.

// let newNumber = [2.3, 3.5, 5.8];
// let number = newNumber.map(Math.round);
// console.log(number);




